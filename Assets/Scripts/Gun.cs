﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private float spawnRate = 0.5f;
    private GameObject shoot;
    private float timer = 0f;
    public bool isPlayerGun = false;
    [SerializeField]
    private float shootSpeed = 0;
    public float criticaleChance = 0.01f;
    public Sprite criticalSprite;
 
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= spawnRate)
        {
            createProjectile();
            timer = 0f;
        }
    }

    void createProjectile()
    {
        shoot = Instantiate(Resources.Load<GameObject>("Shot"), transform.position, transform.rotation);
        if (Random.Range(0f,1f) <= criticaleChance)
        {
            shoot.GetComponentInChildren<SpriteRenderer>().sprite = criticalSprite;
            shoot.GetComponent<Shoot>().damage = (int)Mathf.Ceil(shoot.GetComponent<Shoot>().damage*1.5f);
        }
        Shoot shootScript = shoot.GetComponent<Shoot>();
        shootScript.isPlayerShoot = isPlayerGun;
        if (shootSpeed != 0)
            shootScript.speed = shootSpeed;
        shoot.SetActive(true);
    }
}
