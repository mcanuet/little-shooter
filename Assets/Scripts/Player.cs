﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("player informations")]
    [SerializeField]
    private float speed = 10f;
    [SerializeField]
    private int lifePoint = 5;
    private int startLifePoint;
    [SerializeField]
    private GameObject vesselSprite;

    [Header("gun informations")]
    private int gunLVL = 1;
    [SerializeField]
    private GameObject[] gunLVL2;
    [SerializeField]
    private GameObject[] gunLVL3;

    private GameManager gameManager;

    [Header("move limite")]
    public float leftBound = -7f;
    public float rightBound = 7f;
    public float topBound = -4f;
    public float bottomBound = 4f;

    private void Start()
    {
        startLifePoint = lifePoint;
        try
        {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }
        catch (System.InvalidCastException e){}
    }

    void Update()
    {
        float posx = transform.position.x + (Time.deltaTime * speed * Input.GetAxis("Horizontal"));
        float posy = transform.position.y + (Time.deltaTime * speed * Input.GetAxis("Vertical"));
        transform.position = new Vector3(Mathf.Clamp(posx, leftBound, rightBound), Mathf.Clamp(posy, topBound, bottomBound), transform.position.z);
    }

    void Dead()
    {
        GetComponent<PolygonCollider2D>().enabled = false;
        GameObject deadAnime = Instantiate(Resources.Load<GameObject>("FX/explosion_1"), transform);
        deadAnime.transform.localScale = new Vector3(15, 15, 15);
        vesselSprite.SetActive(false);
        StartCoroutine(SelfDestroy(1f));
        gameManager.EndGame();
    }

    public void TakeDamage(int damageAmount)
    {
        lifePoint -= damageAmount;
        if (lifePoint <= 0)
            Dead();
    }

    public int GetLifePoint()
    {
        return lifePoint;
    }

    public int GetStartLifePoint()
    {
        return startLifePoint;
    }

    private IEnumerator SelfDestroy(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    //enabled and disabled gun
    public void ChangeGunLevel()
    {
        gunLVL++;
        switch (gunLVL)
        {
            case 2:
                ActivateGun(gunLVL2);
                break;
            case 3:
                ActivateGun(gunLVL3);
                break;
            default:
                break;
        }
    }

    private void ActivateGun(GameObject[] guns)
    {
        foreach (GameObject gun in guns)
        {
            gun.SetActive(true);
        }
    }

    public void ResetLifePoint(int newlifePoint)
    {
        lifePoint = newlifePoint;
        startLifePoint = lifePoint;
    }
}
