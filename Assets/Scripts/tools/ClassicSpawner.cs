﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class ClassicSpawner : MonoBehaviour
{
    [SerializeField]
    //private float spawnRate = 0.1f;
    private float waitTime = 1f;
    private float timer = 0f;

    [Serializable]
    private struct enemy
    {
        public GameObject prefab;
        public float chanceToSpawn;
    }

    [Serializable]
    private struct Possibility
    {
        public GameObject prefab;
        public float borneMin;
        public float borneMax;
    }

    [Header("Enemy possibility")]
    [Header("spawn chance [0, 1]")]
    [SerializeField]
    private enemy[] enemyPossibility;
    private List<Possibility> possibilityTable = new List<Possibility>();

    // Start is called before the first frame update
    void Start()
    {
        //waitTime = 1 / spawnRate;
        MakePossibilityTable();
        SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= waitTime)
        {
            SpawnEnemy();
            timer = 0f;
        }
    }

    // make list with good number for random spawn
    void MakePossibilityTable()
    {
        float value = 0f;
        foreach (enemy enemy in enemyPossibility)
        {
            Possibility tempPossibility = new Possibility();
            tempPossibility.prefab = enemy.prefab;
            tempPossibility.borneMin = value;
            tempPossibility.borneMax = value + enemy.chanceToSpawn;
            value += enemy.chanceToSpawn;
            possibilityTable.Add(tempPossibility);
        }

    }

    void SpawnEnemy()
    {
        float randomNumber = UnityEngine.Random.Range(0f, 1f);
        foreach (Possibility possibility in possibilityTable)
        {
            if (randomNumber >= possibility.borneMin && randomNumber <= possibility.borneMax)
            {
                Instantiate(possibility.prefab, new Vector3(0, 0, 0), Quaternion.identity);
            }
        }
    }
}
