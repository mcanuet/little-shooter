﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBezierSpline : MonoBehaviour
{
    [SerializeField]
    private Transform[] splines;
    [SerializeField]
    private float speedModifier = 0.5f;

    private int splineToGo = 0;
    private float tParam = 0f;
    private Vector2 objectPosition;
    private bool coroutineAllowed = true;

    // Update is called once per frame
    void Update()
    {
        if (coroutineAllowed)
            StartCoroutine(MoveOnspline(splineToGo));
    }

    private IEnumerator MoveOnspline(int splineNumber)
    {
        coroutineAllowed = false;

        while (tParam < 1)
        {
            tParam += Time.deltaTime * speedModifier;

            objectPosition = Mathf.Pow(1 - tParam, 3) * splines[splineToGo].GetChild(0).position +
                3 * Mathf.Pow(1 - tParam, 2) * tParam * splines[splineToGo].GetChild(1).position +
                3 * (1 - tParam) * Mathf.Pow(tParam, 2) * splines[splineToGo].GetChild(2).position +
                Mathf.Pow(tParam, 3) * splines[splineToGo].GetChild(3).position;

            transform.position = objectPosition;
            yield return new WaitForEndOfFrame();
        }

        tParam = 0f;
        splineToGo += 1;

        if (splineToGo > splines.Length - 1)
        {
            //splineToGo = 0;
            Destroy(gameObject);
        }

        coroutineAllowed = true;
    }
}
