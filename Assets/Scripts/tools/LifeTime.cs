﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTime : MonoBehaviour
{
    [SerializeField]
    private float lifeTime;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SelfDestroy(lifeTime));
    }

    private IEnumerator SelfDestroy(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }
}
