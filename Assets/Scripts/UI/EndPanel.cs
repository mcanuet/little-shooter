﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;

[Serializable]
class Data
{
    public string username { get; set; }
    public int score { get; set; }
}

public class EndPanel : MonoBehaviour
{
    private List<Data> savedData;
    private GameManager gameManager;

    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private TMPro.TextMeshProUGUI scoreList;
    [SerializeField]
    private GameObject[] elementToUnactivate;

    public void Prepare()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        foreach (GameObject element in elementToUnactivate)
        {
            element.SetActive(false);
        }
        savedData = new List<Data> { };
        Load();
        AddNewScore(PlayerPrefs.GetString("username"), GameObject.Find("GameManager").GetComponent<GameManager>().GetScore());
        ShowScore(PlayerPrefs.GetString("username"), gameManager.GetScore());
        ShowScoreList();
    }

    public void AddNewScore(string username, int score)
    {
        Data newScore = new Data();
        newScore.username = username;
        newScore.score = score;
        savedData.Add(newScore);
        
        savedData = savedData.OrderByDescending(data => data.score).ToList();

        if ( savedData.Count() >= 6)
            savedData.RemoveAt(5);

        Save();
    }

    void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/score.dat");
        bf.Serialize(file, savedData);
        file.Close();
    }

    void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/score.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/score.dat", FileMode.Open);
            savedData = (List<Data>)bf.Deserialize(file);
            file.Close();
        }
    }

    public void ShowScore(string username, int score)
    {
        scoreText.text = username + " : " + score.ToString();
    }

    void ShowScoreList()
    {
        foreach (Data score in savedData)
        {
            scoreList.text += score.username + " : " + score.score.ToString() + " \n ";
        }
    }

    public void Restart()
    {
        gameManager.Restart();
    }
}
