﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanel : MonoBehaviour
{
    private GameManager gameManager;
    private Player player;
    private int lifePoints;
    private int score;

    [SerializeField]
    private Text scorText;
    [SerializeField]
    private GameObject hpBar;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.GetScore() != score)
            UpdateScore();
        if (player.GetLifePoint() != lifePoints)
            UpdateLifePoints();
    }

    void UpdateLifePoints()
    {
        lifePoints = player.GetLifePoint();
        if (lifePoints == 0)
            hpBar.transform.localScale = new Vector2(0, 0);
        else
        {
            hpBar.transform.localScale = new Vector2((float)lifePoints / (float)player.GetStartLifePoint(), hpBar.transform.localScale.y);
        }
    }

    void UpdateScore()
    {
        score = gameManager.GetScore();
        scorText.text = "score: " + score.ToString();
    }
}
