﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StartPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject[] elementToActivate;
    [SerializeField]
    private GameObject[] elementToUnactivate;

    [Header("UI element")]
    [SerializeField]
    private InputField usernameInput;

    public void StartGame()
    {
        PlayerPrefs.SetString("username", usernameInput.text);

        foreach (GameObject element in elementToUnactivate)
        {
            element.SetActive(false);
        }
        foreach (GameObject element in elementToActivate)
        {
            element.SetActive(true);
        }
    }
}
