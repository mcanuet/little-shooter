﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBackground : MonoBehaviour
{
    public float moveSpeed = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0f, moveSpeed, 0));
        if (transform.position.y <= -5f)
        {
            transform.position = new Vector3(transform.position.x, 5f, transform.position.z);
        }
    }
}
