﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private int currentScore = 0;
    [SerializeField]
    private GameObject player;

    [Header("UI")]
    [SerializeField]
    private GameObject startPanel;
    [SerializeField]
    private GameObject gamePanel;
    [SerializeField]
    private GameObject endPanel;

    public void AppendScore(int value)
    {
        currentScore += value;
    }

    public int GetScore()
    {
        return currentScore;
    }

    public void EndGame()
    {
        gamePanel.SetActive(false);
        endPanel.SetActive(true);
        endPanel.GetComponent<EndPanel>().Prepare();
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
