﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float speed = 20;
    public int damage = 1;
    public bool isPlayerShoot = false;

    private void Start()
    {
        StartCoroutine(SelfDestroy(2f));
    }

    // shoot always go forward
    void Update()
    {
        Vector3 newPos = transform.right * Time.deltaTime * speed;
        transform.position = transform.position + newPos;
    }

    private IEnumerator SelfDestroy(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherGO = collision.gameObject;
        if (isPlayerShoot && otherGO.tag == "enemy")
            ShootEnemy(otherGO);
        else if (!isPlayerShoot && otherGO.tag == "player")
            ShootPlayer(otherGO);
    }

    private void ShootEnemy(GameObject enemy)
    {
        EnnemiManager enemyManager = enemy.GetComponent<EnnemiManager>();
        enemyManager.TakeDamage(damage);
        Destroy(gameObject);
    }

    private void ShootPlayer(GameObject player)
    {
        Player playerManager = player.GetComponent<Player>();
        playerManager.TakeDamage(damage);
        Destroy(gameObject);
    }
}
