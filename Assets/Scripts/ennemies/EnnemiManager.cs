﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiManager : MonoBehaviour
{
    [SerializeField]
    private int lifePoint = 1;
    [SerializeField]
    private GameObject vesselSprite;
    [SerializeField]
    private int scoreValue = 100;

    private GameManager gameManager;

    void Start()
    {
        try
        {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }
        catch (System.InvalidCastException e) { }
    }

    void Dead()
    {
        gameManager.AppendScore(scoreValue);
        GetComponent<PolygonCollider2D>().enabled = false;
        Instantiate(Resources.Load<GameObject>("FX/explosion_1"), transform);
        vesselSprite.SetActive(false);
        StartCoroutine(SelfDestroy(.8f));
    }

    public void TakeDamage(int damageAmount)
    {
        lifePoint -= damageAmount;
        if (lifePoint <= 0)
            Dead();
    }

    private IEnumerator SelfDestroy(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(transform.parent.gameObject);
    }
}